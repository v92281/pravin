# -*- coding: utf-8 -*-
##############################################################################
#
#    Sales Extension module for Odoo
#    Copyright (C) 2016 Akili Systems, Inc (http://www.akilisystems.in/)
#    @author: Akili Systems
#
##############################################################################

{
    'name': "Akili Web Extension",
    'summary': """
        Customizations related to Product""",
    'version': '10.0.1.0.0',
    'category': 'Product',
    'website': "http://www.akilisystems.in",
    'author': "Akili Systems (P) LTD",
    'installable': True,
    'depends': ['web', 'web_enterprise', 'website', 'website_enterprise'],
    'data': [
            'views/resources_view.xml',
            'views/webclient_templates.xml',
            ],
    'qweb': [
        "static/src/xml/base.xml",
    ],
}
